# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 16:55:43 2020

@author: Benslimane
"""

import matplotlib.pyplot as plt
import numpy as np
import random as rd
import copy

#%% 
class player:
    def __init__(self, name="unknown", coin="X", p_is_human = True, random_play = True):
        self._name = name
        self._coin=coin
        self._p_is_human = p_is_human
        self._random_play = random_play
        
        self._training = True
        
        self.MIN_EPSILON = 0.05
        self.EPSILON = 0.999999
        self.LEARNING_RATE = 0.1
                
        #Q-learning
        self._Q = {}
        self.init_Q_table()      
    
        #Monte Carlo Tree Search
        self._history = []
        self._MCTS = {}
        self.init_MCTS_table()
        
        
    def init_MCTS_table(self):
        all_states = self.get_all_states(9)
        for state in all_states:
            key = self.convertBoardToKey(state)
            self._MCTS[key] = [0,0]
        
    def init_Q_table(self):
        all_states = self.get_all_states(9)
        for state in all_states:
            key = self.convertBoardToKey(state)
            self._Q[key] = {}
                
        for key in self._Q.keys():
            for action in range(0,9):
                self._Q[key][action] = 0
                        
    def get_all_states(self,n):
        possibilities = [['.'],[self._coin],['O']]
        if(n==1):
            sol = [['.'],[self._coin],['O']]
        else:
            sol = []
            for pos in possibilities:
                next_step = self.get_all_states(n-1)
                for s in next_step:
                    sol.append(pos+s)
        return sol
        
    def convertBoardToKey(self, board):
        return '_'.join(board)
    
    def reversing_board(self, board):
        reverse_board = []
        for p in board:
            if(p==self._coin):
                reverse_board.append('O')
            elif(p=='.'):
                reverse_board.append('.')
            else:
                reverse_board.append(self._coin)
        return reverse_board
    
    def possibilities(self, board):           
        all_possibilities = []
        tempBoard = copy.deepcopy(board)
        for action in range(9):
            if(board[action]=='.'):
                tempBoard[action]=self._coin
                all_possibilities.append((action, tempBoard))
                tempBoard = copy.deepcopy(board)
                
        return all_possibilities 
    
    def turn_off_training(self):
        self._training = False
    
    def get_reward_of_state(self, board):
        #Game finished with a winner
        if(board[0] == board[1] and board[1] == board[2]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1                
        if(board[3] == board[4] and board[4] == board[5]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[6] == board[7] and board[7] == board[8]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[0] == board[3] and board[3] == board[6]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[1] == board[4] and board[4] == board[7]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[2] == board[5] and board[5] == board[8]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[0] == board[4] and board[4] == board[8]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        if(board[2] == board[4] and board[4] == board[6]):
            if(board[0] != '.'):
                if(board[0] == self._coin):
                    return 1
                else:
                    return -1
        
        #Game not finished : Goal -> encourage to 'win' as fast as possible
        for b in board:
            if (b=='.'):
                return -0.001

        #TIE (Decide that a Tie is a good result if it is not possible to win)
        return 0.3
                
#-------------------------
        
    def greedy_choice_q_learning(self, current_board, possibilities): # Greedy step
        v_max = None
        new_board = None
        state_key = self.convertBoardToKey(current_board)

        for poss in possibilities:
            action, next_state = poss
            if(v_max is None or v_max < self._Q[state_key][action]):
                v_max = self._Q[state_key][action]
                
                action_made = action
                new_board = next_state  
        
        return (action_made, new_board)
    
    def greedy_choice_monte_carlo(self, current_board, next_states):
        p_max = -1
        new_board = None

        for poss in next_states:
            key_state = self.convertBoardToKey(poss[1])
            tot_state = self._MCTS[key_state][0]+self._MCTS[key_state][1]
            if(tot_state == 0):
                p_state = 0.00000001
            else:
                p_state = self._MCTS[key_state][0]/tot_state
            
            #test difference 
            if(p_state==p_max):
                if(rd.randint(0, 1)==0): #if two states has same proba, choose it randomly
                    p_max = p_state
                    new_board = poss[1]
            elif(p_state>p_max):
                    p_max = p_state
                    new_board = poss[1]            
            
        return new_board
    
    def playOneTime(self, game):      
        check=False
        
        old_board = copy.deepcopy(game._board)
        new_board = copy.deepcopy(game._board)
        
        #How to play depending on who you are
        if(self._p_is_human==True): #Playing by a human
            game.printBoard()
            while (check==False):
                print("ligne : ")
                l = input()
                print("colonne : ")
                c = input()
                ci = int(c)
                li=int(l)
                pl = ci + 3*li
                if(ci>=0 and ci<3 and li>=0 and li<3 and game._board[pl]=='.'):
                    check=True
                else:
                    print("IMPOSSIBLE play. Re-play")
            new_board[pl]=self._coin
            
        else: 
            if(self._random_play==True): #Random play from the AI random
                while(check==False):
                    pl = rd.randint(0, 8)
                    if(game._board[pl]=='.'):
                        check=True
                new_board[pl]=self._coin
            else:
                if(rd.uniform(0, 1) < self.EPSILON): #random choice
                    while(check==False):
                        action_made = rd.randint(0, 8)
                        if(game._board[action_made]=='.'):
                            check=True
                    new_board[action_made]=self._coin
                else: #greedy choice #[QL]
                    all_poss = self.possibilities(old_board)
#                    action_made, new_board = self.greedy_choice_q_learning(old_board, all_poss)
                    new_board = self.greedy_choice_monte_carlo(old_board, all_poss) #[MCL]
                    
                    
                #Update our Q table [QL]
                if(self._training==True):
#                    self.q_learning_training(old_board, action_made, new_board) #Update our Q table [QL]
                    self._history.append(new_board) #[MCL]

        return new_board
    
    
    """
    def : Q-LEARNING : Reverse the board to find the best choice to make for next step if player was the opponant 
    """
    def q_learning_training(self, bt, at, btp1):
        qt = self._Q[self.convertBoardToKey(bt)][at] #get Q value at Board t and action t
        rt = self.get_reward_of_state(bt) #reward of current board
        
        rev_btp1 = self.reversing_board(btp1) #reverse of board at t+1
        all_poss_revtp1 = self.possibilities(rev_btp1) #get all (action, board_at_t+2)
        
        rev_atp1, _ = self.greedy_choice_q_learning(rev_btp1, all_poss_revtp1) #get best choice action for the opponent
        qtp1 = self._Q[self.convertBoardToKey(rev_btp1)][rev_atp1] #get Q value at Board t+1 and action t+1
        
        #Bellman equation
        self._Q[self.convertBoardToKey(bt)][at] = qt + self.LEARNING_RATE * (rt + self.EPSILON*qtp1 - qt)
        
        return 0       
    
    
    """
    def : Monte-Carlo : backpropagation about results possibilities
    """
    def monte_carlo_training(self, result):
        if(result==p0._coin):
            win=1
            lose=0
        elif(result=='TIE'):
            win=0.5
            lose=0.5
        else:
            win=0
            lose=1
            
        for state in self._history:
            key_state = self.convertBoardToKey(state)
            old_res = self._MCTS[key_state]
            self._MCTS[key_state] = [old_res[0]+win, old_res[1]+lose]
        
        self._history = []
        
        return 0

#%%
class game:
    def __init__(self, p1, p2):
        self._board = ['.','.','.','.','.','.','.','.','.']
        self._p1 = p1
        self._p2 = p2
        self._turn = None
        self._winner = None
        self.initRandomTurn()
   
    def resetGame(self):
        self._board = ['.','.','.','.','.','.','.','.','.']
        self._history = []
        self._turn = None
        self._winner = None
        self.initRandomTurn()
        
    def initRandomTurn(self):
        nb = rd.randint(0, 1)
        if (nb==0):
            self._turn=p1._coin
        else:
            self._turn=p2._coin
    
    def endGame(self):
        if(self._board[0] == self._board[1] and self._board[1] == self._board[2] and self._board[2] != '.'):
            return self._board[0]
        if(self._board[3] == self._board[4] and self._board[4] == self._board[5] and self._board[5] != '.'):
            return self._board[3]
        if(self._board[6] == self._board[7] and self._board[7] == self._board[8] and self._board[8] != '.'):
            return self._board[6]
        if(self._board[0] == self._board[3] and self._board[3] == self._board[6] and self._board[6] != '.'):
            return self._board[0]
        if(self._board[1] == self._board[4] and self._board[4] == self._board[7] and self._board[7] != '.'):
            return self._board[1]
        if(self._board[2] == self._board[5] and self._board[5] == self._board[8] and self._board[8] != '.'):
            return self._board[2]
        if(self._board[0] == self._board[4] and self._board[4] == self._board[8] and self._board[8] != '.'):
            return self._board[0]
        if(self._board[2] == self._board[4] and self._board[4] == self._board[6] and self._board[6] != '.'):
            return self._board[2]
        
        #Game in progress
        for b in self._board:
            if (b=='.'):
                return None
        
        #TIE
        return "TIE"
 
    def printBoard(self):
        print("      0     1     2   ")
        print("   -------------------")
        print("0  | ", self._board[0], " | ", self._board[1]," | ", self._board[2], " |")
        print("   -------------------")
        print("1  | ", self._board[3], " | ", self._board[4]," | ", self._board[5], " |")
        print("   -------------------")
        print("2  | ", self._board[6], " | ", self._board[7]," | ", self._board[8], " |")
        print("   -------------------")          O

#%%
def playTicTacToe(game,p1,p2):
    
    game.resetGame()
    #play going on
    while game._winner==None:
        if(game._turn==p1._coin):
            game._board = p1.playOneTime(game)
            game._turn=p2._coin
        else:
            game._board = p2.playOneTime(game)
            game._turn=p1._coin
        game._winner=game.endGame()
  
    #End of the game
    game.printBoard()
    
#    if(game._winner=="TIE"):
#        print("IT'S A TIE !")
#    else:
#        if(p1._coin==game._winner):
#            name_winner = p1._name
#        else:
#            name_winner = p2._name
#        print("the winner is : ",name_winner)       
    
    return game._winner

#%%
p0b = player("AI QLearning", "X", False, False) #0
p0 = player("AI Monte Carlo", "X", False, False) #0
p1 = player("AI Random", "O", False) #1
p2 = player("Samy", "O") #2
#%%

#regarder les board winning

stats = {}
stats[p0._name]=0
stats[p1._name]=0
stats['TIE']=0
g=game(p1,p2)

max_epoch =100000 #10Mi
for epoch in range(0,max_epoch):
    if (epoch%100==0):
        p0.EPSILON = max(p0.EPSILON*0.996, p0.MIN_EPSILON)   
    winner = playTicTacToe(g,p0,p1)
 
    if(winner==p0._coin):
        stats[p0._name]+=1
    elif(winner==p1._coin):
        stats[p1._name]+=1
    else:
        stats['TIE']+=1
    
    #monte-carlo training
    if(p0._training==True):
        p0.monte_carlo_training(winner)
        
    if (epoch%10000==0):
        print("loading ====", (epoch/max_epoch)*100, "%")